#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <Arduino.h>

#define READYBYTE 1
#define TYPEBYTE 2
#define DATABYTE 3
#define ENDBYTE 4

typedef enum SerialState {
  ReadyState,
  ReadTypeState,
  ReadDataState
};

typedef enum MessageType {
  None,
  Init,
  SetConfig,
  ErrorMessage,
  InfoMessage,
  ScopeData,
  Stop
};

void setupSerial();
void handleSerialData();
void sendScopeData(byte data);
void sendInit();
void sendError(const char c[]);
void sendInfo(const char msg[]);
bool getInitialized();

#endif
