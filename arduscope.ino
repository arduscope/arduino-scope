#include "serialcom.h"

void setup() {
  // put your setup code here, to run once:
  setupSerial();
  sendInfo("ArduScope Setup Run");
  sendInit();
}

void loop() {
  if (getInitialized()){
    sendScopeData(PINB);
  }    
}

void serialEvent() {
  while (Serial.available()) {
    handleSerialData();
  }
}