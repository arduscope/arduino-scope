#include "Arduino.h"
#include "serialcom.h"

enum SerialState state;
enum MessageType type;
bool initialized = false;

void setupSerial(){
  state = ReadyState;
  Serial.begin(500000);
  pinMode(13, INPUT);
  DDRB = 0;  
}

void readReady(){
  int d = Serial.read();
  if (d == READYBYTE) state = ReadTypeState;
  else {
    sendError("Ready not parsed");
    const char dt[1] = { (char)d };
    sendError(dt);
  }
}

void readType(){
  if (Serial.available()<2) return;
  int d = Serial.read();
  if (d == TYPEBYTE) {
    type = Serial.read();
    state = ReadDataState;
  }
}

void readConfigData(){
  state = ReadyState; 
}

void readInitData(){
  if (Serial.available()<1) return;
  state = ReadyState;  
  initialized = true;
  sendInfo("started");  
  byte eb = Serial.read();
}

void readStopData(){
  if (Serial.available()<1) return;
  state = ReadyState;  
  initialized = false;
   sendInfo("stopped");
  byte eb = Serial.read();
}

void readData(){
  switch (type){
    case SetConfig:
      readConfigData();          
      break;
    case Init:
      readInitData();
      break;
    case Stop:
      readStopData();
      break;      
  }
}

void handleSerialData(){
  switch (state) {
    case ReadyState:
      readReady();
    case ReadTypeState:
      readType();
    case ReadDataState:
      readData();
      break;
  }
}

void sendInit(){
  Serial.write(Init);
}

void sendInfo(const char msg[]){
  Serial.write(InfoMessage);
  Serial.print(msg);
  Serial.write(0);
}

void sendError(const char msg[]){
  Serial.write(ErrorMessage);
  Serial.print(msg);
  Serial.write(0);
}
void sendScopeData(byte data){
  Serial.write(ScopeData); 
  Serial.write(data); 
}

bool getInitialized(){
  return initialized;
}